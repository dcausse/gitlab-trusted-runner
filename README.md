# gitlab-trusted-runner

Repository for Trusted GitLab Runner. This project is the root project were Trusted Runners are registered. This project also contains the configuration to allow other projects usage of Trusted Runners.

## Request access to Trusted Runners

Add your project to `projects.json` and create a merge request. Please provide the id of your project (can be found under Settings, General), the name and a reason why access is needed. This helps validate if project should be allowed for Trusted Runners.

## Usage of `add-project.py`

Make sure to export a valid GitLab API token as `GITLAB_TOKEN`.

Basic usage:

```
usage: add-project.py [-h] [--projects [PROJECTS]] [--instance [INSTANCE]] {diff,apply}

positional arguments:
  {diff,apply}          action to perform, either diff or apply

optional arguments:
  -h, --help            show this help message and exit
  --projects [PROJECTS] Filename of configuration file for all projects
  --instance [INSTANCE] hostname of GitLab instance
```

Example to check the diff for a certain configuration:
```
add-project.py diff
```

Output (with color diff):
```
gitlab-runner2001.codfw.wmnet:
+project with id 182 repos / releng / Gitlab Runner Test
-project with id 75 Jelto / test-project

gitlab-runner1001.eqiad.wmnet:
+project with id 182 repos / releng / Gitlab Runner Test
-project with id 75 Jelto / test-project
```

If diff looks good, run the same command with `apply` instead of `diff`:

```
add-project.py apply
```

Note: CI jobs will be available soon.

## Todos:

 - [ ] add tests for code
 - [x] add requirements.txt
 - [x] add .gitlab-ci.yml / CI for this process
 - [ ] add docs in wikitech/phabricator
 - [ ] add pagination for graphql query (max 100 projects are returned)
